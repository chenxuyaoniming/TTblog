

const basePath = process.env.NODE_ENV === 'production' ? '/TTblog/' : '/'

module.exports = {
    title: 'CCblog',
    description: '知识小仓库',
    base: basePath,
    head: [
        ['script', { src: 'https://cdn.jsdelivr.net/npm/react/umd/react.production.min.js' }],
        ['script', { src: 'https://cdn.jsdelivr.net/npm/react-dom/umd/react-dom.production.min.js' }],
        ['script', { src: 'https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js' }],
        ['script', { src: 'https://cdn.jsdelivr.net/npm/@babel/standalone/babel.min.js' }],
    ],
    plugins: [
        'demo-block'
    ],
    theme: 'vuepress-theme-yuu',
    themeConfig: {
        catalogUrl:'/catalog',//必填 目录路径
        lastUpdated: 'Last Updated',
        nav: [
            {
                'text':'个人介绍',
                link: '/aboutMe'
            }, {
                text: '学习',
                link: 'teach'
            }, {
                text: '优秀博客',
                link: '/link'
            }
        ],
        sidebar: [
            '/',
            {
                title: 'HTTP',
                path: '/http',
                children: [
                    '/http/',
                    '/http/http',
                    '/http/enterUrl',
                    '/http/status',
                    '/http/keepAlive'
                ]
            },
            {
                title: 'HTML',
                path: '/html',
                children: [
                    '/html/',
                    '/html/message',
                    '/html/event',
                    '/html/cache',
                    '/html/router',
                    '/html/media',
                    '/html/socket',
                ]
            },
            {
                title: 'CSS',
                path: '/css',
                children: [
                    '/css/',
                    '/css/bfc',
                    '/css/flex'
                ]
            },
            {
                title: 'JS',
                path: '/js',
                children: [
                    '/js/',
                    '/js/array',
                    '/js/prototype',
                    '/js/promise',
                    '/js/designModal',
                    '/js/event',
                    '/js/class',
                    '/js/proxy',
                    '/js/for',
                    '/js/function',
                    '/js/environment'
                ]
            },
            {
                title: 'React',
                path: '/react',
                children: [
                    '/react/',
                    '/react/vdom',
                    '/react/router',
                    '/react/setState',
                    '/react/lifecycle',
                    '/react/getDrived',
                    '/react/stack&fiber',
                    '/react/redux',
                    '/react/mobx',
                    '/react/event',
                    '/react/fiber',
                    '/react/hooks',
                    '/react/createPortal',
                ]
            },
            {
                title: 'Vue',
                path: '/vue',
                children: [
                    '/vue/',
                    '/vue/composition',
                    '/vue/defineProperty',
                    '/vue/vueArray',
                    '/vue/vuex',
                    '/vue/key',
                    '/vue/proxy',
                    '/vue/slot',
                    '/vue/keepAlive',
                    '/vue/nextTick',
                ]
            },
            {
                title: 'Webpack',
                path: '/webpack',
                children: [
                    '/webpack/',
                    '/webpack/loader',
                    '/webpack/plugin',
                    '/webpack/chain'
                ]
            },
            {
                title: 'Plugin',
                path: '/plugin',
                children: [
                    '/plugin/',
                    '/plugin/rollup',
                    '/plugin/babel'
                ]
            },
            {
                title: 'TypeScript',
                path: '/ts',
                children: [
                    '/ts/',
                    '/ts/generic'
                ]
            },
            {
                title: '数据结构与算法',
                path: '/alg',
                children: [
                    '/alg/',
                    '/alg/sort',
                    '/alg/linked',
                    '/alg/hfman',
                    '/alg/binaryTree',
                    '/alg/fibonacci',
                    '/alg/search',
                ]
            },{
                title: '设计模式',
                path: '/design',
                children: [
                    '/design/',
                    '/design/singleton',
                    '/design/observer',
                    '/design/oop',
                    '/design/aop',
                    '/design/functor',
                ]
            },
            {
                title: '面试题',
                path: '/face',
                children: [
                    '/face/',
                    '/face/promise',
                    '/face/layout',
                    '/face/react',
                    '/face/call',
                    '/face/deepClone',
                    '/face/diff',
                    '/face/optimize',
                    '/face/webpack'
                ]
            }
        ]
    },
}