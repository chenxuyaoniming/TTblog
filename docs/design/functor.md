### 函数式编程
通过管道把数据在一系列的纯函数间传递

#### 概念
函数式编程的特点
- 1. 相同的输入有相同的输入
- 2. 对外界没有其他依赖，没有副作用
```js
    const func = (value) => {
        return value + 1;
    }
```

#### 柯里化
函数柯里化的特点
- 1. 缓存：对于初始值进行缓存，下次可直接使用
- 2. 新的函数：柯里化函数会返回一个新的函数
- 3. 闭包：利用了js的闭包特性
```js
    const curry = (init) => {
        return function(x) {
            return init + x;
        }
    }
```

#### 组合函数
- 1. 接受多个函数入参，返回一个新的函数
- 2. 入参函数以函数位置从右到左依次执行
```js
    const compose = (a, b, c) => {
        return function(val) {
            return a(b(c(val)))
        }
    }
```
