#### 面向对象
- 封装
- 继承
- 多态

**es5-组合继承**

```js


function Foo(name) {
    this.name = name;
    this.type = 'foo';
}

Foo.prototype.say = function() {
    console.log(this.name)
}

function boo() {
    this.age = 13;
}

Foo.apply(boo);
boo.prototype = new Foo()

```


**es6-class**

```js
class Foo {
    constructor(name) {
        this.name = name
    }
    say() {
        console.log(this.name)
    }
}

class Boo extends Foo {
    constructor() {
        super();
        this.age = 13;
    }
}

```