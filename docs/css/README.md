#### css基础

#### 行内元素与块元素

#### 水平垂直居中的几种写法

|  |垂直居中|水平居中|水平垂直居中|
| ---- | ---- |----|----|
| 行内元素 | verticle-align: center | text-align:center  | |
| 块级元素 | | margin: 0 auto | |
| flex | justify-content: center | align-item: center |align-item: center;justify-content: center; |
| position | top: 50%; transform: translateY(-50%)| left: 50%; transform: translateX(-50%)| left: 50%; top: 50%; transform: translate(-50%, -50%) |


#### src，href的区别
- src: 是将资源嵌入到昂前文档元素所在的位置。
- href: 指定了web资源的位置。