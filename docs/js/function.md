### 函数

#### 普通函数

- prototype
- arguments
- target
- constructor
- this

#### 箭头函数

- =>
- 无命名
- 无this，继承上一级
- 无arguments
- 无target
- 无constructor
- 无prototype