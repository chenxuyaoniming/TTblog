# js

```js
    const a = 1;

    instanceof 判断某个实例的原型链上是否存在一个构造函数的prototype属性
    
    function > var > arg 提升优先级

```
| 声明类型 | 声明 | 初始化 | 赋值 |
| ---- | ---- | ---- | ---- |
| var |auto |auto | auto|
| let | 手动 | 手动 | 手动 |
| const | 手动 | 手动 | 手动 |

| 类型|内容|
| ---- | ---- |
| 基本类型 | number, string, boolean, undefined, null, Symbol|
| 引用类型 | object, array, function, regExp, set, map|

JS的继承方式：
1. 原型链继承
2. 构造函数继承
3. 组合式集成
4. 原型继承
5. 寄生继承
6. 寄生组合继承  

[继承](https://www.cnblogs.com/ranyonsue/p/11201730.html)