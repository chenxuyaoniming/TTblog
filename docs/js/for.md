
### JS之循环

- for...i++
- for...in
- for...of
- forEach

#### for...i++
break，return中断，i=索引，
```js
    for(var i=0; i<100; i++) {
        console.log(i);
    }
```

#### for...in
break，return中断，i=索引，在遍历对象时，i=key, 语句以任意顺序迭代对象的可枚举属性
```js
    const obj = {
        a: 1,
        b: 2,
        c: 3
    };

    const arr = [1,2,3,4,5];

    for (let i in arr) {
        console.log(i)
    };
    // 0,1,2,3,4
    for (let i in obj) {
        console.log(i)
    };
    // a, b, c
```

#### for...of
遍历具有迭代器接口的数据，包括 Array，Map，Set，String，TypedArray，arguments 对象等等，i=value, 遍历可迭代对象定义要迭代的数据。
```js
    const obj = {
        a: 1,
        b: 2,
        c: 3
    };

    const arr = [1,2,3,4,5];

    for (let i of arr) {
        console.log(i)
    };
    // 1,2,3,4,5
    for (let i of obj) {
        console.log(i)
    };
    // 1, 2, 3
```

#### forEach
遍历数组，不可中断，没有返回值, 参数为一个回调函数，
```js
    const arr = [1,2,3,4,5];
    arr.forEach((item, index) => {
        console.log(item, index)
    })
```