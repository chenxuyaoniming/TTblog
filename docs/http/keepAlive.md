### keep-alive与keepAlive

- Http之keep-alive：客户端在和服务端交互时，如果客户端发送的请求头connection:keep-alive，并且服务端接受了这个字段，那么这个连接就可以复用，避免了重连造成的延迟
- TCP之keepAlive: 保持客户端与服务端的连接，当双方长时间没有数据传输时，会发送keepalive探针，检测连接是否存活，连接是否需要关闭

<font color='red'>**tcp的keepalive是在ESTABLISH状态的时候，双方如何检测连接的可用行。而http的keep-alive说的是如何避免进行重复的TCP三次握手和四次挥手的环节。**</font>