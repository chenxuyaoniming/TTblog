### 浅克隆和深克隆

- 浅克隆 拷贝一层
    - Object.assign
    - [ ...array ]

- 深克隆 拷贝多层
    - JSON.parse(JSON.stringify(data))

#### JSON.parse(JSON.stringify())
优点：简单实现深拷贝的方式将数据序列化后在转换成对象，可实现深拷贝
缺点：对数据格式有限制，数据内的函数。undefined会丢失

#### 手写深克隆

```js

function myClone(obj) {
    const mapKeys = new WeakMap();    
    const deepClone = (data) => {
        if (!data || (typeof data !== 'object')) return data;
        if (mapKeys.get(data)) {
            return mapKeys.get(data);
        }
        mapKeys.set(data, data);
        const newData = Array.isArray(data) ? [] : {};
        for(let key in data) {
            const val = data[key];
            newData[key] = deepClone(val);
        }
        return newData;
    }

    return deepClone(obj);
}
```