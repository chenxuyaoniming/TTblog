### Diff算法
react和vue的diff算法思想基本一致

diff算法的前提假设
- 假设dom没有跨层级移动
- 同层节点使用key值进行比对

#### **React diff**
diff算法的策略
- dom节点的跨层级操作是非常少的，可以忽略不计
- 相同类型的组件会生成类似的结构树，不同类型的组件生成的结构树基本不一致
- 对于同一层的一组子节点，可以通过唯一id进行区分

diff算法的步骤
- 组件树的diff （tree diff）
- 组件的diff （component diff）
- 同一层级下的节点diff （element diff）

tree diff  
- 只对相同层级的节点进行diff计算，跨层级的不考虑，当检测到节点不存在时，直接删除该节点及其子节点，
- 如果出现跨层级的组件移动，直接视为原组件的删除，和新组建的渲染
- 优化： 跨层级的移动，十分影响渲染性能，尽量减少dom的跨层级操作

component diff
- 判断两个组件是否是同一个，如果是，向下继续diff，如果不是，则直接删除该组件，生成新的组件
- 优化：同类型的组件，可以通过生命周期 shouldComponentUpdate 进行判断，是否进行更新，减少diff过程，提高性能

element diff
- 相同层级的节点diff操作操作有三种（通过key值进行判断）对比方式为顺序对比，1.插入 2.移动 3.删除 
- 插入： 新的组件类型不在旧的组件集合内，则会对新节点执行插入操作
- 移动：旧节点存在新组件的类型，且element是可更新的，则会复用旧节点的
- 删除：旧组件的类型在新组件内有，但是不能直接复用，或者旧组件不在新组件的集合内，则会执行删除操作
- 优化：通过给节点添加唯一的key值，可以快速判定元素的插入，移动，删除操作，提升diff性能，

#### **Vue diff**
vue的diff算法和react基本一致，差别主要是两点
- vue比对节点，当节点元素类型相同，但是className不同，认为是不同类型元素，删除重建，而react会认为是同类型节点，只是修改节点属性

- vue的列表比对，采用从两端到中间的比对方式，而react则采用从左到右依次比对的方式。当一个集合，只是把最后一个节点移动到了第一个，react会把前面的节点依次移动，而vue只会把最后一个节点移动到第一个

