### 项目优化方式

#### vue

#### react

- 使用纯组件 pureComponent  
- 使用React.memo 主要用作函数组件  
- 使用shouldComponetupdate  
- 使用React.Fragments 无节点避免额外标记  
- 不要使用内联函数定义，就是render里，组件的onClick直接()=>{this.xxx},因为每次调用render，都会创建一个新的函数实例，因此，直接绑定内联函数就会需要额外的做绑定到DOM操作和回收  
- 使用箭头函数，因为使用箭头函数会绑定上下文，就不需要将函数绑定到constructor了  
- 避免使用内联样式，因为使用内联样式需要花费更多时间处理脚本和渲染，因为要映射传递到实际的CSS属性  
- 优化react的条件渲染  
- 使用唯一键迭代，尽量不要使用默认的index，因为diff算法会去遍历新旧两个对象，如果使用index，在表头新增一个，那么所有的index都+1，所有的都需要重新更新。  
- 事件的防抖和节流  
- 前后端数据传输，如果数据包太大，前端可以用gzip进行压缩  
- 使用suspense组件进行组件的懒加载

#### webpack

- 图片压缩：image-webpack-loader压缩图片资源
- 减少语法转译后的冗余代码：babel-plugin-transform-runtime
- 提取公共代码和第三方依赖的代码: 将公共模块单独打包成一个common.js，将一些体积较大的库单独进行打包（在入口配置或者在optimize下配置）
- 提取公共css，注入模块：将一些公共样式单独写成一个文件，然后通过webpack注入打包后的css文件内
- source-map优化：配置它方便查找问题，不同环境下使用不同的source-map配置，优化打包流程 prod：cheap-module-eval-source-map, dev：cheap-module-source-map
- 构建结果分析：配置webpack-bundle-analyzer，根据图表在进项webpack其他的打包优化。
- cdn引入：将一些第三方库用cdn的方式引入，可减少项目的打包体积

#### 首页优化

- html保留一个loading样式
- 骨架屏
- 数据异步加载
- 图片懒加载
- 模块的按需加载
- 减少首页模块的体积
- 缩小js，css资源体积