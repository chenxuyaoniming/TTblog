### 面试题

1. promise并发限流
2. webpack打包
3. webpack ：plugin&loader区别
4. 实现一个循环引用的深克隆 
```js
    let a = {a: 1};
    a.b = a;
```
#### CDN
CDN: 内容分发网络，在不同地域的用户访问网站的响应速度存在差异,为了提高用户访问的响应速度、优化现有Internet中信息的流动,需要在用户和服务器间加入中间层CDN. 使用户能以最快的速度，从最接近用户的地方获得所需的信息，彻底解决网络拥塞，提高响应速度，是目前大型网站使用的流行的应用方案.

#### onclick和addeventlistener区别
- 事件绑定
    - addeventlistener可以绑定多个事件，执行顺序冲上到下
    - onclick只能绑定一个事件，多个事件会被覆盖，
- 执行时机
    - addeventlistener可以控制事件的执行时机 捕获/冒泡阶段
    - onclick无法控制事件的执行
- 元素绑定
    - addeventlistener可以绑定任何DOM元素，且有效
    - onclick在某些元素上不生效
- 事件移除
    - addeventlistener -> removeeventlistener
    - onclick -> onclick = null
- 事件等级
    - addeventlistener为DOM2级事件
    - onclick为DOM0级事件