### Babel - javaScript编译器

babel是一个工具链，主要用于将采用es6以上语法编写的代码转换为向后兼容的javaScript语法，以便能够运行在当前和旧版本或其他环境中  
主要功能：

- 语法转换
- 通过Polyfill方式在目标环境中添加缺失的特性（通过第三方polyfill模块，例如code-js，实现）
- 源码转换（codemods）

特性：

- 插件化
- 可调式
- 编辑结果符合规范
- 代码紧凑

```js
    // 转换前 箭头函数
    [1, 2, 3].map(n => n + 1)
    // 转换后
    [1, 2, 3].map(function(n) {
        return n + 1;
    })

```

#### babel使用流程
运行安装babel所需的的包
```sh
    # // shell
    npm install -D @babel/core @babel/core @babel/preset-env
```
创建babel配置文件
- babel.config.json（建议使用）: 单一仓库，需要编译node_modules
- .babelrc.json: 仅适用于项目的某个部分
```js
    // babel.config.json
    {
        "presets": [
            [
                "babel/env",
                {
                    "targets": {
                        "edge": "17",
                        "firefox": "60",
                        "chrome": "67",
                    },
                    "useBuiltIns": "usage",
                    "corejs": "3.6.5"
                }
            ],

        ]
        "plugins": [
            ...
        ]
    }
```

#### babel的插件和预设
- presets: 预设，babel官方已经写好的编译插件
- plugins: 当预设无法满足项目需求时，可以通过插件形式继续添加编译方式
- 预设的执行顺序是从后往前，c -> b -> a
- 插件会在预设之前执行，插件自身的执行顺序是从前往后 a -> b -> c

```js
    {
        "presets": [
            "presetA",
            ["presetB"],
            ["presetC", {
                // something
            }]
        ],
        "plugins": [
            // 如上
            "pluginA",
            ["pluginB"],
            ["pluginC", {}]
        ]
    }
```

#### 插件的开发
```js
    export default function() {
        return {
            visitor: {
            Identifier(path) {
                const name = path.node.name;
                // reverse the name: JavaScript -> tpircSavaJ
                path.node.name = name
                .split("")
                .reverse()
                .join("");
            },
            },
        };
    }
```

#### [babel官网](https://www.babeljs.cn/docs/index.html)