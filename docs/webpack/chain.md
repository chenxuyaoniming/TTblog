#### webpack-chain
- chain配置  

[chain官方文档](https://github.com/neutrinojs/webpack-chain/tree/v4)
```js
// yarn add webpack-chain

const Chain = require('webpack-chain');

const config = new Chain();

config
    // 设置入口
    .entry('main')
        .add('src/index.js')
    // 设置出口
    .output
        .path('dist')
        .filename('[name].bundle.js')
config.module
        .rule('vue')
        .test(/\.vue$/)
        .pre()
        .include
            // 进入include配置
            .add('src')
            .end()
            // 倒退到上一步 
        .use('vue-loader')
            // 添加loader
        .options({
            // 规则
            presets: [
                ['@babel/preset-env', { modules: false }]
            ]
        })

config.resolve
        .alias
        // 设置路径别名
        .set('@', './src')
        // 清除别名
        .delete('@')
        // 清空alias
        .clean()

// 配置插件
config.plugin('clean')
        .use(CleanWepackPlugin, {root: '/dir'});

module.exports = config.toConfig();
```
**简单的chain配置项目**
```js
const Chain = require('webpack-chain');
const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');

const config = new Chain();

config.entry('index')
        .add('./src/index.js')
    .end()
    .output
    .path(path.resolve(__dirname, 'dist'))
    .filename('[name].bundle.[hash:8].js')

config.devServer
    .port(3000)
    .hot(true)
    .compress(true)

config.resolve
    .alias
    .set('@', './src')

config.plugin('html-template')
    .use(htmlWebpackPlugin, [{
        template: path.resolve(__dirname, './public/index.html'),
        filename: path.resolve(__dirname, './dist/index.html'),
        chunks:'all'
    }])

config.optimization
    .splitChunks({
        cacheGroups: {
            index: {
                name: 'index', // 打包后的文件名
                chunks: 'initial', //
                minChunks: 2,
                maxInitialRequests: 5,
                minSize: 0,
                priority: 1,
                reuseExistingChunk: true
            },
            vendors: {
                name: 'vendors',
                test: /[\\/]node_modules[\\/]/,
                chunks: 'initial',
                priority: 2,
                reuseExistingChunk: true,
                enforce: true
            }
        }
    })


module.exports = config.toConfig();

```