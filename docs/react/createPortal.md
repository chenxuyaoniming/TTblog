### React Portal 传送门
react-portal传送门用于在组件树以外渲染dom

#### 为什么要在组件树以外渲染内容
实际开发中父子组件的层级的相互作用以及样式的影响，我们可能需要将子组件绑定到其他dom元素上来实现正常的功能，我们可以使用以下方式来进行渲染
```js
    const modal = () => {
        ref.current = document.createElement('div');
        const content = (<div>this is modal</div>);
        document.body.appendChild(ref.current);
        React.render(content, ref.current);

    }
```
这种方式可以解决问题，不够优雅，每次组件的展示与隐藏，都要去创建元素，删除元素，影响性能，且在react内部动态创建元素的这种方式，也是不提倡的。

#### ReactDOM.createPortal
ReactDOM.createPortal是官方推出的用于在组件树以外渲染组件的API，Portal组件只在首次调用时会去创建一个dom容器，后续就不会再次去创建：
```js
import { createPortal } from 'react-dom';

const Portal = ({children, isOpen}: PropsType) => {
    const ref = useRef<HTMLElement>();

    useEffect(() => {
    if (ref.current) {
        return;
    }
    ref.current = document.createElement('div');
    ref.current.classList.add('cf-modal');
    document.body.appendChild(ref.current);
    }, [])

    useEffect(() => {
        console.log(children);
    }, [children])

    return (ref.current 
        ? ReactDOM.createPortal((
            <div className={'cf-modal-content'} aria-hidden={isOpen}>
                {children}
            </div>
            ), ref.current) 
        : null)
}
const App = () => {

    const [count, setCount] = useState(0)
    useEffect(() => {
        setTimeout(() => {
        setCount(100);
        }, 1000);
    }, [])

    const handleClick = () => {
        setCount(0)
    }

    return (
        <div className="App" onClick={handleClick}>
            <Portal isOpen={count > 0}>
                {
                <div>
                    portal is good
                </div>
                }
            </Portal> 
        </div>
    )
}
```

#### tips
1. <font color='red'>首次渲染出现子组件不显示的情况，我们需要给子组件加一个是否渲染的状态</font>    
2. <font color='red'>以上示例，react版本是16.3以上</font> 
