### react-fiber
在react16.0以上 通过performUnitOfWork方法操控的工作单元，通过fiber架构，实现了对组件更新的调度。包含跟踪，暂停，终止，重启。

#### fiberNode
一个用来模拟调用栈的链表结构. 我们在遍历 dom 树 diff 的时候，即使中断了，我们只需要记住中断时候的那么一个节点，就可以在下个时间片恢复继续遍历并 diff。这就是 fiber 数据结构选用链表的一大好处
```js
    const fiber = {
        type: '类型',
        return: '父节点',
        sibling: '右侧兄弟节点',
        alternate: '旧树的对应节点',
    }

    调度中心(schedule，任务队列，任务优先级配置) ---> 协调（reconciliation， 可打断） ----> render（转换虚拟dom）
```
#### fiber架构的两个阶段
- reconciliation（协调,过程可中断，频繁触发）:  
    - 生命周期:
        - shouldComponentUpdate
        - static getDerivedStateFromProps
    - 特点：
        - 可以被暂停，中断，重启，恢复，相应的钩子函数也会多次执行，为了减少副作用，删除了componentWillMount，componentWillReceiveProps, componentWillUpdate可以获取组件实例的钩子方法
        - 深度优先搜索算法，先处理子节点，在处理兄弟节点。
- commit (提交, 不可中断)
    - 生命周期：
        - componentDidMount
        - componentWillUpdate
        - getSnapshowBeforeUpdate
    - 特点：
        - 删除了componentWillUpdate
        - 这一过程是同步的，不可暂停，撤销

#### 调度任务的优先级
- 根据任务类型不同，设置了任务的优先级，不同优先级的任务给到的执行时间是不同的

#### fiber调度过程
- scheduleUpdateOnFiber（入口）
    - markUpdateTimeFromFiberToRoot：更新fiber节点的过期时间
    - ensureRootIsScheduled：将同步任务和异步任务推入任务队列
    - schedulePendingInteractions：调用scheduleInteractions
    - scheduleInteractions： 会利用FiberRoot的 pendingInteractionMap 属性和不同的 expirationTime，获取每次schedule所需的update任务的集合，记录它们的数量，并检测这些任务是否会出错

#### 总结
- fiber其实就是一个节点，是链表的遍历形式
- fiber 通过优先级计算 expirationTime 得到过期时间
- 因为链表结构所以时间切片可以做到很方便的中断和恢复
- 时间切片的实现是通过 settimeout + postMessage 实现的
- 当所有任务都延迟时会执行 clearTimeout
- 任务数 和 工作时间的计算