### React Hooks

- **useEffect**: 组件变动时执行的副作用
- **useState**: 组件状态的声明
- **useReducer**: 根据类型，修改对应的值
- **useMemo**: 惰性变量，当依赖项变动时才会重新计算
- **useCallback**: 惰性函数，当依赖项变动时会重新生成新的函数
- **useRef**: 保存引用（dom，timer）

#### **useEffect**
函数组件初始化后执行的副作用，监听变量的修改执行回调函数
```js
    const Page = ({data}) => {
        // 组件每次渲染都会执行，因为没有依赖项
        useEffect(() => {
            // dosomething
        })
        // 组件初始化后执行一次，后续组件的渲染将不会再执行它
        useEffect(() => {
            // do something
        }, [])
        // 组件初始化时执行一次，后续当data变动时，在执行它
        useEffect(() => {
            // do something
        }, [data])
        // useEffect可以反悔一个方法，当组件销毁时会执行它
        useEffect(() => {
            window.addEventListener('scroll', function...)
            return () => {
                window.removeEventListener('scroll', function...)
            }
        }, [])
        return <div></div>
    }
```

#### **useState**
函数组件的状态声明
```js
    const Page = () => {
        // useState接受一个参数，函数（函数必须有返回值）或者变量，作为初始值，
        // 返回一个数组arr，arr[0]是当前state的值，arr[1]是修改state的方法
        const [count, setCount] = useState(0);
        // const [count, setCount] = useState(() => 0);
        const handleClick = () => {
            setCount(count + 1);
        }
        // 执行修改state方法是异步行为，无法在当前函数内获取最新的state，State的修改方法，提供第二个参数（函数），当setState执行完后，会执行该方法，将修改的最新值作为入参
        const handleClick = () => {
            setCount(count + 1, (val) => {
                console.log(val)
            })
            // 获取不到最新值
            console.log(count);
        }
        return <div>
            <button onClick={handleClick}>click</button>
        </div>
    }
```

#### **useMemo**
该方法返回一个值，只有对应的依赖修改时，才会重新计算，可以优化组件性能
```js
    const Page = () => {
        const [count, setCount] = useState(0);
        const [color, setColor] = useState('green');
        // newCount首次计算的值被缓存下来，只有当count变动时，才会重新计算新的newCount
        const newCount = useMemo(() => count*10, [count]);

        return <div>
            <p>{newCount}</p>
            <button onClick={() => {setCount(count + 1)}}>change count</button>
            // setColor时，newCount的依赖没有改变，不会重新计算
            <button onClick={() => {setColor(color + '#')}}>change color</button>
        <div>
    }
```
#### **useCallback**
该方法返回一个函数，一般用作固定穿参，只有当依赖项变动时，才会重新生成新的函数
```js
    const Page = () => {
        const handleChange = (type, value) => {
            switch(type) {
                case 'click':
                    ...
                    break;
                case 'input':
                    ...
                    break;
            }
        }
        const handleClick = useCallback((value) => {
            return handleChange('click', value);
        }, [])
        const handleInput = useCallback((value) => {
            return handleChange('input', value);
        }, [])
        return <div>
            <button onClick={handleClick}></button>
            <input onChange={handleInput}>
        </div>
    }
```