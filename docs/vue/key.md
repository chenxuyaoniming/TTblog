#### VUE :Key
- diff算法的同层比较
- 循环组件绑定key值可以在数据发生改变时减轻渲染压力，提升性能
- 高效的更新虚拟DOM
- vue中在使用相同标签名元素的过渡切换时，也会使用到key属性，其目的也是为了让vue可以区分他们

**插入元素**  
![](https://img2020.cnblogs.com/blog/615223/202006/615223-20200628101623785-664613291.png)  
**不加key的diff**
![](https://img2020.cnblogs.com/blog/615223/202006/615223-20200628101722320-1674277859.png)
**加了Key的diff**  
![](https://img2020.cnblogs.com/blog/615223/202006/615223-20200628101804170-857159246.png)

添加key值之后，同层对比时会对key值进行比较，如果只是位置变动，key值不变，那么vue会直接将组件位置修改。如果key值不同，则会重新渲染组件

**绑定Key的方式**
- index与当前元素进行拼接(不保险)
```html
<template>
    <div>
        <component v-for="(item, index) in data" 
                   :key="`${item.data}:${index}`">
        </component>
    </div>
</template>
```
- 自定义返回唯一key的方法
```js
function getKey() {
    const mapKey = new WeakMap();
    let count = 0;
    return (data) => {
        const key = mapKey.get(data);
        if (key) {
            return key;
        } else {
            const newKey = ++count;
            mapKey.set(data, newKey);
            return newKey;
        }
    }
}
```
