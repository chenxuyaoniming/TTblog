
### VUE之nextTick

- dom更新是异步的
- 获取更新后的dom需要在下一次event loop中进行
- nextTick本质上是用promise，MessageChannel或者settimeout，将函数执行时机放入下一次event loop中
- 建议所有的dom操作都放在nextTick下


|操作|event| loop| 说明 |
| ---- | ---- | ---- | ---- |
|数据变更|同步| 1| 数据变更是，同步去修改数据，并且派发notify批量更新dom |
|dom更新|异步| 1| 更新操作进入任务队列，待主线程同步任务执行完毕之后，再执行任务内的微任务 |
|nextTick获取dom|异步|2| nextTick将函数执行放入下一次eventLoop中 |
