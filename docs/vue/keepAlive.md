### Vue:keep-alive 组件缓存
缓存组件，避免多次渲染，减轻页面渲染压力
- 被keep-alive标签包裹的组件，会将自身的vnode缓存起来。当再次渲染此组件时，直接将上次缓存的vnode返回
- 被keep-alive标签包裹的组件，会多出两个钩子函数activited&deactivated
    - activated: 组件被缓存时执行
    - deadctivated：页面切换时执行（相当于正常组件的destoryed方法），取消监听
- keep-alive属性（字符串或正则，匹配组件名称，优先以组件对外暴露的name为准，否则查找路由name）：
    - exclude: 匹配上的组件不会被缓存
    = include: 匹配上的组件才可以被缓存
      
用法：
```html
    <keep-alive exclude="Home" include="Page">
        <Home/>
<!--    <Page/>-->
    </keep-alive>
```
