### Vue-slot: 插槽
当两个组件具有相似功能或者样式时，可以封装相同的部分，再把不同的地方以插槽形式放入其中，提高开发效率，和组件的可复用性

#### 默认插槽
```html
    // component a
    <template>
        <div>
            <slot></slot>
            // this is slot
        </div>
    </template>
    // page
    <template>
        <div>
            <component-a>
                <div>
                    this is slot
                </div>
            </component-a>
        </div>
    </template>
```

#### 具名插槽
```html
<!--    Content-->
    <div>
        <slot is="content"></slot>
    </div>
<!--    common component-->
    <div>
        <content>
            <div slot="content">
                this is name slot
            </div>
        </content>
    </div>
```
#### 动态插槽
```html
<!--slot-->
    <div>
        <slot :is="slotName"></slot>
    </div>
```
```js
export default {
    data() {
        return {
            slotName: 'content'
        }
    },
    methods: {
        changeSlot() {
            this.slotName = 'btn'
        }
    }
}
// slot
```
```html
    <div>
        <content>
            <div slot="content">this. is content</div>
            <div slot="btn">submit</div>
        </content>
    </div>
```