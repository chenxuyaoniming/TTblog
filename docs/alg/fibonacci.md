### 斐波那契
一个数列，从第三项开始，每一项都等于前两项之和
0，1，1，2，3，5，8，13，21，34，55...

#### 实现
```js
    function fibo(n) {
        if (n <= 1) {
            return n;
        }
        return fibo(n-1) + fibo(n-2);
    }

```
#### 尾递归优化
```js
    function fibo(n, num = 0, sum = 1) {
        if (n <= 1) {
            return sum;
        }
        return fibo(n-1, sum, num + sum);
    }
```
#### 走台阶问题 （递归+动态规划）
一步上一阶，一步上两阶 。。。。。
只有1格的时候。只能走1步。。。。就1种
只有2格的时候，可以1+1||2.。。。2种
3格的时候，1+1+1||2+1||1+2.。。3种
4格的时候1+1+1+1||2+2||2+1+1||1+1+2||1+2+1。。。5种

```js
    function step(n) {
        if (n === 1) {
            return 1;
        }
        if (n === 2) {
            return 2;
        } 
        if (n > 2) {
            return step(n - 1) + step(n - 2);
        }
    }
```
优化
```js
function step2(n, s = 1, sum = 2) {
    if (n === 1 || n === 2) {
        return sum;
    }
    if (n > 2) {
        return step2(n - 1, sum, sum + s);
    }
}
```