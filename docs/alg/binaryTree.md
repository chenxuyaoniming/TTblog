### 二叉树
树是非顺序数据结构。树型结构是一类非常重要的非线性结构。直观地，树型结构是以分支关系定义的层次结构
- 二叉树创建
- 二叉树取值-最大最小
- 二叉树反转
- 二叉树前续遍历: 根节点 -> 左子树 -> 右子树
- 二叉树中续遍历: 左子树 -> 跟节点 -> 右子树
- 二叉树后续遍历: 左子树 -> 右子树 -> 跟节点
- 二叉树层序遍历: 同层遍历，依次向下
- 二叉树深度遍历: 优先往树结构最深处遍历
- 二叉树广度遍历: 从根节点开始，自上而下逐层遍历，同层从左到右

```js
class TreeNode {
    constructor(val, left = null, right = null) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}


class Tree {
    constructor() {
        this.head = null
    }
    // 二叉树构建
    setNode(el) {
        let treeNode = new TreeNode(el);
        let current = this.head;
        if (!this.head) {
            this.head = treeNode;
        } else {
            while(current) {
                if (current.val >= el) {
                    if (!current.left) {
                        current.left = treeNode
                        current = null
                    } else {
                        current = current.left;
                    }
                } else {
                    if (!current.right) {
                        current.right = treeNode
                        current = null
                    } else {
                        current = current.right;
                    }
                }
            }
        }
        
    }
    create(arr) {
        arr.forEach(element => {
            this.setNode(element)
        });
    }
    // 反转二叉树
    reservs() {
        const resversNode = (node) => {
            if (node) {
                const {left, right, val} = node;
                return {
                    val,
                    right: resversNode(left), 
                    left: resversNode(right)
                };
            }
        }

        return resversNode(this.head)
    }
    // 获取最大
    getMax() {
        let current = this.head;
        let max;
        while(current) {
            max = current.key
            current = current.right;
        }
        return max;
    }
    // 获取最小
    getMin() {
        let current = this.head;
        let min;
        while(current) {
            min = current.key
            current = current.left;
        }
        return min;
    }
     // 前序遍历
    getBefore() {
        const arr = [];
        const sort = (node) => {
            if (node) {
                const {left, right, val} = node;
                return [val, ...sort(left), ...sort(right)]
            }
            return [];
        }
        return sort(this.head);
    }
        // 中序遍历
    getCenter() {
        const arr = [];
        const sort = (node) => {
            if (node) {
                const {left, right, val} = node;
                return [...sort(left), val, ...sort(right)]
            }
            return [];
        }
        return sort(this.head);
    }
    // 后续遍历
    getAfter() {
        const arr = [];
        const sort = (node) => {
            if (node) {
                const {left, right, val} = node;
                return [...sort(left), ...sort(right), val]
            }
            return [];
        }
        return sort(this.head);
    }

    // 层序遍历
    getFloorSort() {
        let res = [];
        const run = (node, index) => {
            if (!res[index]) {
                res[index] = [];
            }
            res[index].push(node.val);
            run(node.left, index + 1);
            run(node.right, index + 1);
        }
        run(this.head, 0);
        return res.flat();
    }
}
```