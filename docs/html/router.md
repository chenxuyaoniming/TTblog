#### 前端路由

**实现逻辑**  
- 拦截用户的刷新操作，避免服务端盲目响应、返回不符合预期的资源内容，把刷新这个动作完全放到前端逻辑里消化掉
- 感知 URL 的变化。这里不是说要改造 URL、凭空制造出 N 个 URL 来。而是说 URL 还是那个 URL，只不过我们可以给它做一些微小的处理，这些处理并不会影响 URL 本身的性质，不会影响服务器对它的识别，只有我们前端能感知到。一旦我们感知到了，我们就根据这些变化、用 JS 去给它生成不同的内容

**实现方式**
- **hash**
- **history**

### **hash**  

hash模式依靠的是onhashchange()事件去监听location.hash的改变。
比如这个 URL：http://www.aaa.com/#/hello，hash 的值为 #/hello。它的特点在于：hash 虽然出现在 URL 中，但不会被包括在 HTTP 请求中，对后端完全没有影响，因此改变 hash 不会重新加载页面。
```js
    window.location.hash = '/index';

    window.addEventListener('hashchange', function() {
        // 监听hash变化
    }, false);
```

### **history**  

利用了 HTML5 History Interface 中新增的 pushState() 和 replaceState() 方法。（需要特定浏览器支持）
pushState()方法可以改变URL地址且不会发送请求，replaceState()方法可以读取历史记录栈，还可以对浏览器记录进行修改。
这两个方法应用于浏览器的历史记录栈，在当前已有的 back、forward、go 的基础之上，它们提供了对历史记录进行修改的功能。只是当它们执行修改时，虽然改变了当前的 URL，但浏览器不会立即向后端发送请求。
```js
    html5 history-api
    pushState replaceState go forward back

    // 监听手动的前进后退 go forward back
    window.addEventListener('popstate', function(event) {
        console.log(event)
    }, false)

    replaceState 替换当前历史记录
    pushState 新增一条历史记录
```

**history模式的问题**  

通过history api，我们丢掉了丑陋的#，但是它也有个问题：<font color="red">不怕前进，不怕后退，就怕刷新，f5，（如果后端没有准备的话）,因为刷新是实实在在地去请求服务器的。</font>
在hash模式下，前端路由修改的是#中的信息，而浏览器请求时不会将 # 后面的数据发送到后台，所以没有问题。但是在history下，你可以自由的修改path，当刷新时，如果服务器中没有相应的响应或者资源，则会刷新出来404页面。