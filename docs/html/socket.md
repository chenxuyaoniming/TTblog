### socket页面通信
- socket
- websocket
- socket.io

#### socket
Socket是传输控制层协议，Socket其实并不是一个协议，而是为了方便使用TCP或UDP而抽象出来的一层，是位于应用层和传输控制层之间的一组接口。可以方便的使用TCP和UDP协议。

####v websocket
h5新增的通信协议，建立了客户端和服务端的长链接，实现了双向数据传输
- tcp连接
- 双向通信
- ws协议
```js
const ws = new Websocket('ws//:xx.xx', [protocol]);

ws.onopen = function() {

}

ws.onmessage = function() {

}

ws.onclose = function() {

}

ws.onerror = function() {

}
```

#### socket.io
一个封装了websocket，ajax和其他通信方式的js库，抹平浏览器兼容性
- Socket.io不是Websocket，它只是将Websocket和轮询 （Polling）机制以及其它的实时通信方式封装成了通用的接口，并且在服务端实现了这些实时机制的相应代码。也就是说，Websocket仅仅是 Socket.io实现实时通信的一个子集。因此Websocket客户端连接不上Socket.io服务端，当然Socket.io客户端也连接不上Websocket服务端。
```js
// server
let express = require('express')
let path = require('path')
let app = express()
let server = require('http').createServer(app)
let io = require('socket.io')(server)

app.use('/', (req, res, next) => {
    res.status(200).sendFile(path.resolve(__dirname, 'index.html'))
})

io.on('connection', client => {
    console.log(client.id, '=======================')
    client.on('channel', data =>{
        console.log(data)
        io.emit('broadcast', data)
        // client.emit('channel', data)
    })
    client.on('disconnect', () =>{
        console.log('close')
    })
})

server.listen(3000, () => {
    console.log("The service listening on 3000 port")
})
```
```js
// client
    window.onload = () => {
        let inputValue = null

        let socket = io('http://localhost:3000')
        socket.on('broadcast', data =>{
            let content = document.createElement('p')
            content.innerHTML = data
            document.querySelector('#content-wrap').appendChild(content)
        })
        
        let inputChangeHandle = (ev) => {
            inputValue = ev.target.value
        }
        let inputDom = document.querySelector("#input")
        inputDom.addEventListener('input', inputChangeHandle, false)

        let sendHandle = () => {                
            socket.emit('channel', inputValue)
        }
        let btnDom = document.querySelector("#btn")
        btnDom.addEventListener('click', sendHandle, false)
        window.onunload = () => {
            btnDom.removeEventListener('click', sendHandle, false)
            inputDom.removeEventListener('input', inputChangeHandle, false)
        }
    }
```

