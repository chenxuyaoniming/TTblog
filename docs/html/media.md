### 响应式布局

#### media相关属性
- 媒体类型：
	- all:所有媒体（默认值
	- screen:彩色屏幕.
	- print:打印预览.
	- projection:手持设备.
    - tv:电视.
	- braille:盲文触觉设备.
	- embossed:盲文打印机.
	- speech:屏幕阅读器等发声设备.
	- tty:不适用像素的设备.
- 媒体属性:
	- <font color='red'>width:浏览器窗口的尺寸（可加max min前缀</font>
		- min-width:100px	  >=100px
		- max-width:100px   <=100px
	- height: 浏览器窗口的尺寸（可加max min前缀）.
	- <font color='red'>device-width: 设备独立像素（可加max min前缀</font>
		- pc端: 分辨率.
		- 移动端: 具体看机器的参数.
	- device-pixel-ratio:屏幕可见宽度与高度的比率/像素比（可加max min前缀，需要加-webkit前缀）.
		- pc端: 1.
		- 移动端: 具体看机器的参数（DPR）.
	- <font color='red'>orientation: portrait(竖屏) | landscape(横屏)</font>
- 操作符，关键字（only,and,(, or),not）
	- only: 和浏览器兼容性有关，<font color='red'>老版本的浏览器只支持媒体类型，不支持带媒体属性的查询</font>
		- @media only screen and (min-width:800px){css-style规则}
		- @media screen and (min-width:800px){css-style规则}
		- 在老款的浏览器下:
			- @media only --> 因为没有only这种设备,规则被忽略.
			- @media screen --> 因为有screen这种设备而且老浏览器会忽略带媒体属性的查询，执行相应的样式.
		- <font color='red'>建议在每次抒写@media 媒体类型的时候带上only</font>
	- nd: 代表与的意思，一般用and来连接媒体类型和媒体属性.类似JS里的&&.
	- or(,): 和and相似.对于所有的连接选项只要匹配成功一个就能应用规则.类似JS里的||.
	- not: 取反,类似JS里的!.

#### 媒体类型
通过媒体类型的指定，可在对应设备上渲染出对应的css样式
```css
// 彩色屏幕
@media screen {
    body {
        width: 100px;
    }
}
// 打印预览
@media print {
    body {
        width: 1000px;
        background: #000;
    }
}
// 所有设备
@media all {
    body {
        font-size: 20px;
    }
}
....
```

#### 媒体属性
设置媒体查询的属性，可以指定各种尺寸下的样式的css，当页面的尺寸可以匹配多个响应式时，css会自上向下依次覆盖
```css
/* 1 */
@media only screen and (min-width: 448px) and (max-width: 768px) {
    body {
        padding: 20px;
    }
}
/* 2 */
@media only screen and (min-width: 768px) {
    body {
        font-size: 20px
    }
}
/* 3 */
@media only screen and (min-width: 1366px) {
    body {
        background: red;
    }
}
/* 4 */
@media only screen and (min-width: 1366px) and (orientation: landscape) {
    body {
        background: red;
    }
}

/* 当页面宽度大于448px 小于768px时，展示1样式 */
/* 页面宽度大于768px小于1366时，展示2样式*/
/* 页面宽度大雨1366px时，会同时展示2.3样式，如果2，3有相同属性，3会覆盖2*/
/* 页面是横屏且宽度大于1366时显示4样式 */
```

#### 操作符
常用only 可在低版本浏览器下渲染渲染有效的css样式