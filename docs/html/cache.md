### 缓存

网络
- 强缓存
- 协商缓存

本地
- cookie
- localStorage
- sessionStorage


**强缓存**  
cache-control优先，cache-control 的max-age字段表示缓存时间（秒），expires保存一个服务器返回的过期时间（相对服务器的时间）  

**协商缓存**  

If-None-Match: 对应的服务端的ETag，用来匹配文件内容是否改变。保存资源ID  
If-Modified-Since: 对应服务端你的Last-Modified, 用来匹配文件是否变动，只能精确到1s之内，保存资源修改时间


**优先级**  

cache-control  > expires > If-Modified-Since = If-None-Match